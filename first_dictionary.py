#encoding: utf-8

#鍵（Key）值（Value）對應的物件，鍵物件必須是 hashable
passwd = {'Mars': 00000, 'Mark': 56680}
passwd['Happy'] = 9999
passwd['Smile'] = 123456

del passwd['Mars']
passwd['Mark'] = passwd['Mark'] + 1

print(passwd)
print(passwd.keys())
print(list(passwd.keys()))
print(passwd.items())
print(passwd.values())
#指定鍵不存在時傳回的預設值
print(passwd.get('Tony','00000'))
print(passwd.get('Mark'))