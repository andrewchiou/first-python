# encoding:utf-8

import sys
from time import time

sys.stdout.write(str(time()))
print()

import xmath
print(xmath.pi)

import xmath as math  # 為 xmath 模組取別名為 math
print(math.e)

from xmath import min  # 將 min 複製至目前模組，不建議 from modu import *，易造>成名稱衝突
print(min(10, 5))

print(list.__doc__)
print()
print(help(list))
print(help(xmath.max))