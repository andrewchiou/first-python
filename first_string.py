# coding=UTF-8
import sys

s = "台灣"

print(s[0])

text = '測試'
print(type(text))  # 顯示 "<class 'str'>"
print(len(text))  # 顯示 2

text2 = b'Andrew'
print(type(text2))
print(len(text2))

print('邱'.encode('UTF-8'))

print("Just'in")
print('Just"in')
print('c:\workspace')
print("c:\workspace")
print("c:\\workspace")
print(r"c:\workspace")

name = 'Justin'
for ch in name:
    print(ch)
print('Just' in name)
print(name * 3)

lang = 'Python'
print(lang[-1])
print(lang[1:5])  # 取得索引 1 至 5（包括 1 但不包括 5）的子字串
print(lang[0:])  # 省略結尾索引，表示取至尾端
print(lang[:6])  # 省略起始索引，表示從 0 開始
print(lang[0:6:2])  # [] 還可以指定間距（Gap）
print(lang[::-1])  # 反轉字串

# 使用字串的 format 方法來取代 % 操作
print('{0} is {1}'.format('Justin', 'caterpillar'))
print('{real} is {nick}'.format(real='Justin', nick='caterpillar'))
print('My platform is {pc.platform}'.format(pc=sys))
