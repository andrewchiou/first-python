#encoding: utf-8


def my_function(x, y):
    return x - 10, y + 10


x, y = my_function(10, 20)
print(x, y)


def max(a, b):
    return a if a > b else b


# 函式不單只是定義，也是個值
maximum = max
print(maximum(10, 20))

#λ 函式或是匿名函式（Anonymous function）
min = lambda a, b: a if a < b else b
print(min(10, 20))
