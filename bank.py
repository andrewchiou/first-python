"""Summary of module here.

Account class
"""


class Account:
    """Summary of class here.

    Longer class information....
    """
    # 首個參數「必定」接受物件本身，慣例上使用 self 名稱

    def __init__(self, name, number, balance):
        self.name = name
        self.number = number
        self.balance = balance

    def deposit(self, amount):
        """Summary of method here.

        """
        if amount <= 0:
            raise ValueError('amount must be positive')
        self.balance += amount

    def withdraw(self, amount):
        if amount > self.balance:
            raise RuntimeError('balance not enough')
        self.balance -= amount

    def __str__(self):
        return 'Account({0}, {1}, {2})'.format(self.name, self.number, self.balance)
