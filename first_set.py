#encoding: utf-8

#set 型態是無序群集，管理的元素不會重複而且必須是 hashable
admins = set()
users = {'Smile', 'Tony', 'Happy', 'Sherry', 'Allen', 'Andy', 'Mars'}
admins.add('ihc')
admins.add('Mars')

print('Mars' in admins)

print(admins & users)
print(admins | users)
print(admins ^ users)
print(admins - users)
print(users - admins)
