names = ['Justin', 'caterpillar', 'openhome']
for i in range(len(names)):
    print('{0}, {1}'.format(i, names[i]))
    
for i, name in zip(range(len(names)), names):
    print('{0}, {1}'.format(i, name))
    
for i, name in enumerate(names):
    print('{0}, {1}'.format(i, name))

print(list(filter(lambda ele: len(ele) > 6, names)))
print(list(filter(lambda ele: 'i' in ele, names)))
print(list(map(lambda ele: ele.upper(), names)))
print(list(map(lambda ele: len(ele), names)))

print([ele.upper() for ele in names])