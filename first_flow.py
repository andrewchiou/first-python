#encoding: utf-8
from sys import argv


my_list = []
for i in range(0, 10):
    my_list.append(i + 1)

if my_list[0] == 1 and len(my_list) < 10:
    my_list[0] += 1
    print("1 state")
elif (10 in my_list) or not(len(my_list) == 10):
    print("2 state")
else:
    print("3 state")

for i in my_list:
    print(i)

# if 條件式成立的話，會傳回 if 左邊的值，否則傳回 else 右邊的值
print('Hello,', argv[1] if len(argv) > 1 else 'Guest')

print('Enter two numbers...')
m = int(input('Number 1: '))
n = int(input('Number 2: '))
while n != 0:
    r = m % n
    m = n
    n = r
print('GCD: {0}'.format(m))

# for 包含式
numbers = [10, 20, 30]
print([number ** 2 for number in numbers])

# for 包含式也可以與條件式結合
numbers = [11, 2, 45, 1, 6, 3, 7, 8, 9]
print([number for number in numbers if number % 2 != 0])

# for 包含式（comprehension）也可以形式巢狀結構
lts = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
print([ele for lt in lts for ele in lt])

# 如果使用 {} 的話，可以建立 set 實例，重複的元素會自動去除
print({name for name in ['caterpillar', 'Justin', 'caterpillar', 'openhome']})

names = ['caterpillar', 'justin', 'openhome']
passwds = [123456, 654321, 13579]
print({name: passwd for name, passwd in zip(names, passwds)})
