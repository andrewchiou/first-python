# encoding: utf-8

# list 是有序且可變群集（Collection）

my_list = []
my_list.append(1)
my_list.append(2)
my_list2 = [55.55, "Hi", 3, 99, 222, 222]
my_list2[0] = 333.333

print(len(my_list), sum(my_list), my_list2.count(222))
print(my_list2[0], my_list2[-1], my_list2[1:3], my_list2[2:])

print([0] * 10)
print(', '.join(['justin', 'caterpillar', 'openhome']))
print(list('justin'))
