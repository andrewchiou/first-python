# encoding:utf-8

x = 2 * 3
y = 3 / 2
s = "3"
print(ord('a'), ord('c'), chr(ord('a') + 2))
print(y, int(s) / 2, float(s) / 2, 3 % 2)
print(str(x + y), str(x) + str(y))
