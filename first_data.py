# encoding: utf-8
import decimal

print(type(1))
print(type(11111111111111111111111111111111111111111))
print(type(3.14))
print(type(True))
print(type(3 + 4j))

# 在 Python 3.x 中，/ 一定是產生浮點數，而 // 的話整數與整數運算會產生整數，如果是整數與浮點數進行 // 會產生浮點數
print(10 / 3)
print(10 // 3)

print(1.0 - 0.8)
print(str(1.0 - 0.8))
print(repr(1.0 - 0.8))

a = decimal.Decimal('1.0')
b = decimal.Decimal('0.8')
print(a - b)
