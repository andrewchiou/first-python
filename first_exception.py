#encoding: utf-8


def my_divide():
    try:
        10 / 0
    except ZeroDivisionError:
        print("不能除以0")
    else:
        print("沒有任何錯誤")
    finally:
        print("一定會執行這行")


my_divide()
